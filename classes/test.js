module.exports = class Test {
    constructor(description, func) {
        this.description = description;
        this.func = func;
    }

    async Execute() {
        try {
            console.log(this.description);
            return await this.func();
        }
        catch (e) {
            return false;
        }
    }
}