const ERRORS = require('./constants/errors');
const WHERE = require('./constants/where');
const IS = require('./constants/is');

const Element = require('./element');
const ElementGroup = require('./elementGroup');

module.exports = class Page {
    constructor(url, driver) {
        this.url = url;
        this.driver = driver;
        this.elementGroups = [];
        this.elements = [];
    }

    async Open() {
        if (this.url == null) throw new Error(`There's no url for page`);
        if (this.driver == null) throw new Error (`There's no driver for page with URL: ${this.url}`);
        
        let response = await request(this.url);
        if (response.statusCode != 200)
            throw new Error (`Server responsed with code ${response.statusCode}`);
        await this.driver.get(this.url);
        return this;
    }

    async GetElement(by) {
        if (by == null) 
            throw new Error(`There's no selector info in GetElement call`);

        let element = await this.driver.findElement(by);
        this.elements[by] = new Element(element, this);
        return this.elements[by];
    }

    async GetElements(by) {
        if (by == null)
            throw new Error(`There's no selector info in GetElements call`);
        
        let elements = await this.driver.findElements(by);
        for (let i = 0; i < elements.length; i++)
            elements[i] = new Element(elements[i], this);
        this.elementGroups.push(new ElementGroup(elements, this));
        return this.elementGroups[this.elementGroups.length-1];
    }
}