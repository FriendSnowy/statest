const ERRORS = require('./constants/errors');
const WHERE = require('./constants/where');
const IS = require('./constants/is');

module.exports = class ElementGroup {
    constructor(elements, page) {
        this.elements = elements;
        this.page = page;
    }

    //Фильтрует элементы группы и возвращает массив элементов
    async Filter(where, attribute, is, value) {
        if (where == null || where == undefined || attribute == null || attribute == undefined
        || is == null || is == undefined || value == null || value == undefined) {
            throw new Error(`Not all parameteres given to Filter function`);
        }
        
        let foundedElements = [];
        for (let element of this.elements) {
            let foundedValue = null;
            switch (where) {
                case WHERE.ATTRIBUTE:
                    foundedValue = await element.getAttribute(attribute);
                    break;
                case WHERE.CSSPARAM:
                    foundedValue = await element.getCssValue(attribute);
                    break;
                default:
                    throw new Error(`WHERE parameter is wrong in Filter function`)
                    break;
            }

            if (foundedValue == null || foundedValue == undefined)
                continue;

            let isElementOK = false;
            switch (is) {
                case IS.CONTAIN:
                    if (foundedValue.includes(value))
                        isElementOK = true;
                    break;
                case IS.EQUALS:
                    if (foundedValue == value)
                        isElementOK = true;
                    break;
                case IS.NOT_CONTAIN:
                    if (!foundedValue.includes(value))
                        isElementOK = true;
                    break;
                case IS.NOT_EQUALS:
                    if (foundedValue != value)
                        isElementOK = true;
                    break;
                case IS.STRICT_IN:
                    if (Array.isArray(value)) {
                        for (let i = 0; i < value.length; i++) {
                            if (foundedValue === value[i]) {
                                isElementOK = true;
                                break;
                            }
                        }
                    }
                    else
                        throw new Error(`You can't use "STRICT_IN" filter without array in a value parameter`)
                    break;
                case IS.STRICT_NOT_IN:
                    if (Array.isArray(value)) {
                        for (let i = 0; i < value.length; i++) {
                            if (foundedValue !== value[i]) {
                                isElementOK = true;
                                break;
                            }
                        }
                    }
                    else
                        throw new Error(`You can't use "STRICT_NOT_IN" filter without array in a value parameter`)
                    break;
                case IS.IN:
                    if (Array.isArray(value)) {
                        let isFounded = false;
                        for (let i = 0; i < value.length; i++) {
                            if (foundedValue.includes(value[i]))
                                isFounded = true;
                            else
                                continue;
                        }
                        isElementOK = isFounded;
                    }
                    else
                        throw new Error(`You can't use "IN" filter without array in a value parameter`)
                    break;
                case IS.NOT_IN:
                    if (Array.isArray(value)) {
                        let isFounded = false;
                        for (let i = 0; i < value.length; i++) {
                            if (foundedValue.includes(value[i]))
                                isFounded = true;
                            else
                                continue;
                        }
                        isElementOK = !isFounded;
                    }
                    else
                        throw new Error(`You can't use "NOT_IN" filter without array in a value parameter`)
                    break;
                default:
                    throw new Error(`IS parameter is wrong in Filter function`)
                    break;
            }

            if (isElementOK) 
                foundedElements.push(element);
        }
        return foundedElements;
    }

    //Фильтрует элементы группы и записывает вернувшийся массив в elements текущей группы, возвращает указатель на группу
    async FilterAndUpdate(where, attribute, is, value) {
        this.elements = await this.Filter(where, attribute, is, value);
        return this;
    }

    //Объединяет массивы elements двух групп, указатель page остается на текущей группе. Возвращает указатель на объединенную группу
    async Union (anotherGroup) {
        let elements = this.elements;
        anotherGroup.elements.forEach((el) => {
            if (!elements.includes(el))
                elements.push(el);
        });
        console.log(elements.length);
        this.elements = elements;
        return this;
    }

    //Получает из всех элементов группы уникальные по аттрибуту элементы и возвращает массив уникальных элементов
    async Unique(where, attribute) {
        if (where == null || where == undefined || attribute == null || attribute == undefined) {
            throw new Error(`Not all parameteres given to MakeUnique function`);
        } 

        let uniqueElements = [];
        for (let i = 0; i < this.elements.length; i++) {
            let searchAttribute = null;

            switch (where) {
                case (WHERE.ATTRIBUTE):
                    searchAttribute = await this.elements[i].getAttribute(attribute);
                    break;
                case (WHERE.CSSPARAM):
                    searchAttribute = await this.elements[i].getCssValue(attribute);
                    break;
                default:
                    break;
            }

            if (uniqueElements.length === 0) {
                uniqueElements.push(this.elements[i]);
            }
            else {
                let attributeWasFounded = false;
                
                for (let j = 0; j < uniqueElements.length; j++) {
                    let foundedAttribute = null;
                    switch (where) {
                        case WHERE.ATTRIBUTE:
                            foundedAttribute = await uniqueElements[j].getAttribute(attribute);
                            break;
                        case WHERE.CSSPARAM:
                            foundedAttribute = await uniqueElements[j].getCssValue(attribute);
                            break;
                        default:
                            break;
                    }

                    if (searchAttribute === foundedAttribute) {
                        attributeWasFounded = true;
                        break;
                    }
                }

                if (!attributeWasFounded)
                    uniqueElements.push(this.elements[i]);
            }
        }

        return uniqueElements;
    }
    //Выполнять Unique и обновляет текущий список элементов группы на результат выполнения 
    async UniqueAndUpdate(where, attribute) {
        this.elements = await this.Unique(where, attribute);
        return this;
    }
    //Получает значения атрибутов элементов и возвращает массив этиз щначений
    async GetAttributeValues(where, attribute) {
        if (where == null || where == undefined || attribute == null || attribute == undefined) {
            throw new Error(`Not all parameteres given to MakeUnique function`);
        } 

        let attributeValues = [];

        for (let i = 0; i < this.elements.length; i++) {
            let el = this.elements[i];
            let foundedAttribute = null;
            switch (where) {
                case WHERE.ATTRIBUTE:
                    foundedAttribute = await el.getAttribute(attribute);
                    break;
                case WHERE.CSSPARAM:
                    foundedAttribute = await el.getCssValue(attribute);
                    break;
                default:
                    break;
            }
            attributeValues.push(foundedAttribute);
        }
        return attributeValues;
    }
}