const ERRORS = require('./constants/errors');
const WHERE = require('./constants/where');
const IS = require('./constants/is');
const Page = require('./page');

module.exports = class Browser {
    constructor(name = null) {
        this.driver = null;

        this.history = {
            pages: [],
            add: (page) => { this.history.pages.push(page); }
        }

        this.GetDriver(name);
    }

    GetDriver (name) {
        let isNameValid = false;
        for (var browserName in webdriver.Browser) {
            if (name === webdriver.Browser[browserName]) {
                isNameValid = true;
                break;
            }
        }

        if (isNameValid)
            this.driver = builder.forBrowser(name).build();
        else
            throw new Error('Browser name is not correct');
    }

    async OpenPage(pageURL) {
        if (this.driver == null || this.driver == undefined)
            throw new Error (`This browser have not his own driver`);

        let page = new Page(pageURL, this.driver);
        await page.Open();
        return page;
    }
}