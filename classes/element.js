const STATES = require('./constants/states');

module.exports = class Element {
    constructor (instance, page) {
        this.instance = instance;
        this.page = page;

        this.state = {
            isKnown : (state) => {
                return (this.state[state] != null || this.state[state] != undefined)
            },
            update : (state, value) => {
                //console.log(`Updating state parameter ${state}`);
                //console.log(`Was '${this.state[state]}', now is '${value}'`);
                this.state[state] = value;
            }
        }
    }

    //Получает атрибут объекта и обновляет его стейт
    async getAttribute(attr) {
        let value = await this.instance.getAttribute(attr);
        if (value == null || value == undefined)
            return null;
        this.state.update(attr, value);
        return value;
    }

    //Получает css св-во объекта
    async getCssValue(attr) {
        return await this.instance.getCssValue(attr);
    }

    //Получает состояние видимости объекта
    async isVisible() {
        this.state.update(STATES.isVisible, await this.instance.isDisplayed());
        return this.state[STATES.isVisible];
    }

    //Получает состояние включенности объекта
    async isEnabled() {
        this.state.update(STATES.isEnabled, await this.instance.isEnabled());
        return this.state[STATES.isEnabled];
    }

    //Осуществляет клик по объекту
    async Click() { 
        if (!this.state.isKnown[STATES.isVisible])
            await this.isVisible();
        if (this.state[STATES.isVisible])
            await this.instance.click();
        else
            throw new Error(ERRORS.elemIsNotVisible);
    }

    //Пишет текст в объект
    async Write(text = ERRORS.inputIsEmpty) {
        if (!this.state.isKnown[STATES.isVisible])
            await this.isVisible();
        if (!this.state.isKnown[STATES.isEnabled])
            await this.isEnabled();

        if (this.state[STATES.isVisible] && this.state[STATES.isEnabled])
            await this.instance.sendKeys(text);
        else {
            if (!this.state[STATES.isVisible])
                throw new Error(ERRORS.elemIsNotVisible);
            if (!this.state[STATES.isEnabled])
                throw new Error(ERRORS.elemInNotEnabled);
        }
    }
}