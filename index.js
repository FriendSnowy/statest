webdriver = require('selenium-webdriver'),
By = webdriver.By,
until = webdriver.until,
builder = new webdriver.Builder(),
request = require('async-request');

const ERRORS = require('./classes/constants/errors');
const WHERE = require('./classes/constants/where');
const IS = require('./classes/constants/is');
const Element = require('./classes/element');
const ElementGroup = require('./classes/elementGroup');
const Page = require('./classes/page');
const Browser = require('./classes/browser');
const Test = require('./classes/test');

var chrome = new Browser('chrome');

let test = new Test("Get unique links", async () => {
    let page = await chrome.OpenPage('https://itsfitness.ru');
    let a = await page.GetElements(By.css('a'));
    await a.UniqueAndUpdate(WHERE.ATTRIBUTE, 'href');
    await a.FilterAndUpdate(WHERE.ATTRIBUTE, 'href', IS.IN, ['tel', 'mailto']);
    let links = await a.GetAttributeValues(WHERE.ATTRIBUTE, 'href');
    return links.length > 3;
});

module.exports.test = test;